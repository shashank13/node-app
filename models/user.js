const mongoose = require('mongoose')

const userSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    contact: {
        type: String,
        require: false
    },
    email: {
        type: String,
        require: false
    }
})

module.exports = mongoose.model('User' , userSchema)