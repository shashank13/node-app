const express = require('express');
const mongoose = require('mongoose');


const app = express();

const mongoUrl = 'mongodb://localhost:27017/app1-db'
mongoose.connect(mongoUrl, {useNewUrlParser:true})
const con = mongoose.connection
con.on('open', ()=>{
    console.log('Connected');
})

app.use(express.json())

const mainRouter = require('./routes/routes')

app.listen(8080, ()=> {
    console.log('Hi I am running on port : 8080')
})

app.use('/users',mainRouter)

app.get('/', (req,res)=>{
    // res.send(JSON.stringify(app._router.stack))
    res.send("<div style='margin:40vh auto'><h1 style='text-align: center;font-size:5rem;'>Welcome to Node Test Application.</h1></div><br><ul><li><a href='/users'>Users</a></li>")
})

app.get('/about', (req,res)=>{
    res.send("Hi I am Shashank Shekhar ")
})

app.get('/test1', (req,res)=>{
    db.collection(col_name).find().toArray((err, result)=>{
        if(err){
            throw err
        } else {
            res.send(result)
        }
    })
})


// MongoClient.connect(mongoUrl,(err, client)=>{
//     if(err) console.log(err);
//     db = client.db('app1-db');
//     app.listen(PORT,(err) =>{
//         if(err) throw err;
//         console.log('Hi I am running on port : '+PORT)
//     })
// })