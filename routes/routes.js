var express = require('express');
var app = express();
const router = express.Router()
const User = require('../models/user')

router.get('/', async(req, res) => {
    try{
        const users = await User.find()
        const result = {data: users}
        res.json(result)
    } catch(err){
        res.send('Error' +err)
    }

})

router.get('/:id', async(req, res) => {
    try{
        const user = await User.findById(req.params.id)
        const result = {data: user}
        res.json(result)
        
    } catch(err){
        res.send('Error' +err)
    }

})

router.post('/add', async(req, res) => {
    console.log(req.body);
    const user = new User({
        name: req.body.name,
        contact: req.body.contact,
        email: req.body.email
    })
    try{
        const a1 = await user.save()
        res.json(a1)
    }catch(err){
        console.log(err);
    }
})

module.exports = router